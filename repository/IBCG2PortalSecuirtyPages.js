/**
 * Created by Nishant-Kolte on 12/12/2017.
 */
module.exports = {
    sections: {
        LoginPage: {
            selector: 'body',
            elements: {
                Username: {locateStrategy: 'xpath', selector: "//input[@id='usernameId']"},
                Password: {locateStrategy: 'xpath', selector: "//input[@id='passwordId']"},
                Submitbutton: {locateStrategy: 'xpath', selector: "//input[@type='submit' and @value='Submit »']"},
                Logoutwarning: {
                    locateStrategy: 'xpath',
                    selector: "//td[contains(text(),'You have now logged out. We recommend that you close your Web browser to protect your personal information.')]"
                },
                invalidcredswarning: {
                    locateStrategy: 'xpath',
                    selector: "//td[contains(text(),'The User Name and Password entered is not valid. Please try again.')]"
                },
                accountlockwarning: {
                    locateStrategy: 'xpath',
                    selector: '//td[contains(text(),"You have exceeded the maximum number of login attempts. Please call your plan\'s toll free number for assistance.")]'
                },
                timeoutmessage: {
                    locateStrategy: 'xpath',
                    selector: '//td[contains(text(),"Your session has timed out.  For your security, your session has been terminated.  Please log in again to access your account.")]'
                },
                Sitecore_username: {
                    locateStrategy: 'xpath',
                    selector: '//td[@id="UserNameLabel"]'
                },



            }
        },
        HomePage: {
            selector: 'body',
            elements: {
                Logoutlink: {locateStrategy: 'xpath', selector: "//*[contains(text(),'Log Out')]"},
                Hometab: {locateStrategy: 'xpath', selector: "//span[contains(text(),'Home')]"},
                Wealthtab: {
                    locateStrategy: 'xpath',
                    selector: '//span[text()=\"Wealth\"][@class=\"tabnavbar_navigator_level1_link_items\"]'
                },

            }
        },
        AdminPage: {
            selector: 'body',
            elements: {
                EmployeeSearchlink: {locateStrategy: 'xpath', selector: "//*[text()=\'Employee Search\']"},

            }


        }
    }
}
